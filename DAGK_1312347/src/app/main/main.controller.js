(function() {
  'use strict';

  angular
    .module('myNewProject')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr) {
    var vm = this;
    vm.professional = [{
            name: 'Adobe Photoshop',
            value: '85'
        }, {
            name: 'HTML',
            value: '90'
        }, {
            name: 'CSS',
            value: '80'
        }, {
            name: 'Javascript',
            value: '100'
        }, {
            name: 'NodeJS',
            value: '95'
        }];


    vm.awesomeThings = [];
    vm.menu = [];
     vm.lgs = [];
    vm.classAnimation = '';
    vm.creationDate = 1461514186622;
    vm.showToastr = showToastr;

    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();

      });



    }
    function getWebDevMenu() {
      vm.menu = webDevMenu.getMenu();

      angular.forEach(vm.menu, function(menu) {
        menu.rank = Math.random();

      });
    }

    function getWebDevLgs() {
      vm.lgs = webDevLgs.getLgs();

      angular.forEach(vm.lgs, function(lgs) {
        lgs.rank = Math.random();

      });
    }


  }
})();
