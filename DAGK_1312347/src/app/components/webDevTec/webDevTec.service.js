(function() {
  'use strict';

  angular
      .module('myNewProject')
      .service('webDevTec', webDevTec);

  /** @ngInject */


  function webDevTec() {
    var data = [
      {
      title:'JAVA',
      number:'65%'
    },
     {
      title:'C++',
      number:'80%'
    },
     {
      title:'PHP',
      number:'70%'
    },
     {
      title:'HTML',
      number:'55%'
    }
    ];
    
    this.getTec = getTec;

    function getTec() {
      return data;
    }
  }

function webDevLgs(){
    var data = [
    {
      title:'JAVA',
      number:'65%'
    },
     {
      title:'C++',
      number:'80%'
    },
     {
      title:'PHP',
      number:'70%'
    },
     {
      title:'HTML',
      number:'55%'
    }];
    this.getLgs = getLgs;

    function getLgs() {
      return data;
    }
  }

  
  function webDevMenu(){
    var data = [
    {
      title:'Information',
      link:''
    },
     {
      title:'Contact',
      link:''
    }];
    this.getMenu = getMenu;

    function getMenu() {
      return data;
    }
  }

})();


